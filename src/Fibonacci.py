import sys


def fibonacci(n):
    """ Find the nth term in the Fibonacci sequence.

    Args:
        n (int): The ordinal of the term to look for in the Fibonacci sequence.
    """
    if n < 0 or n % 1 != 0:
        print('Input must be a non-negative integer')
        sys.exit()

    elif n <= 1:
        return 1
    else:
        return fibonacci(n - 2) + fibonacci(n - 1)


if len(sys.argv) != 2:
    print('Usage Error: python Fibonacci.py NUMBER')
    sys.exit()


ordinal = int(sys.argv[1])
print(fibonacci(ordinal))
