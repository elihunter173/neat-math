import sys 


def factorial(n):
    """Return factorial of n.

    Args:
        n (int): The integer to find the factorial of
    """
    if (n < 1):
        return 1
    else:
        return n * factorial(n - 1)


if (len(sys.argv) != 2):
    print('Usage Error: python Factorial.py NUMBER')
    sys.exit()


number = int(sys.argv[1])
print(factorial(number))
