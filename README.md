# Neat Math

## Authors

- Eli W. Hunter

## Installation

Clone the repository and then run the desired math function in the src directory. (eg. `src/Factorial.py`)

```shell
git clone https://gitlab.com/elihunter173/neat-math.git
cd neat-math
python src/MathFunction.py
```

## Built With

- [Python](https://www.python.org/) - A simple, whitespace based, object-oriented programming language.
